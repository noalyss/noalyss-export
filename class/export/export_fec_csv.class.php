<?php
namespace NoalyssExport;
/*
*   This file is part of Noalyss.
*
*   Noalyss is free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   Noalyss is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with Noalyss; if not, write to the Free Software
*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
// Copyright (2020) Author Dany De Bontridder <danydb@noalyss.eu>

require_once DIR_EXPORT_ACCOUNT.'/class/export/export.class.php';
require_once DIR_EXPORT_ACCOUNT.'/class/export/output_csv.class.php';

class Export_FEC_CSV extends Export
{
    private $ledger_code; //!< Code journal
    private $ledger_label; //!< libellé du journal
    private $sequence ; //!< sequence de l'écriture comptable (jr_id ou j_id ?)
    private $recording_date; //!< date comptabilisation
    private $account_code;  //!< poste comptable
    private $account_label;  //!< libellé poste comptable
    private $account_auxiliaire ; //!< poste comptable auxiliaire
    private $account_auxiliaire_label ; //!< poste comptable auxiliaire
    private $receipt; //!< reference pièce justificative
    private $receipt_date; //!< date de la  pièce
    private $recording_label; //!< Libellé de l'opération
    private $amount_debit; //!< montant debit
    private $amount_credit; //!< montant credit
    private $lettering ; //!< code lettrage
    private $lettering_date ; //!< date lettrage
    private $validate_date ; //!< date validation
    private $currency_amount; //!< montant en devise
    private $currency_code ; //!< Code le devise
    private $num_siren; //!< num SIREN
    // + 3 champs pour agricole
    private $payment_date; //!< Date paiement
    private $payment_code; //!< Code paiement
    private $operation_type; //!< nature operation

    //+1 champs pour comptabilité de trésorerie
    private $customer_id; //! Identification client


    public function __construct()
    {
        parent::__construct();
    }
    private function fetch_data()
    {
        $cn=\Dossier::connect();
        $operation=$cn->get_array("
                    select
                    jrn_def_code,
                    jrn_def_name,
                    jr_id,
                    to_char(EcritureDate::date,'YYYYMMDD') as EcritureDateStr,
                    j_poste,
                    pcm_lib,
                    j_qcode,
                    f_id_name,
                    jr_pj_number,
                    to_char(PieceDate::date,'YYYYMMDD') as pj_number_date,
                    jr_comment,
                    amount_debit,
                    amount_credit,
                    lettering,
                     to_char(lettering_date::date,'YYYYMMDD') as lettering_date,
                     to_char(validate_date::date,'YYYYMMDD') as validate_date,
                    case when oc_amount = 0 then null else oc_amount end as oc_amount,
                    case when cr_code_iso='EUR' then null else cr_code_iso end as cr_code_iso
                    from noaexport.v_fec_operation 
                    where 
                          EcritureDate >= to_date($1,'DD.MM.YYYY')
                      and    EcritureDate <= to_date($2,'DD.MM.YYYY')
                      and (amount_debit > 0 or amount_credit > 0)
                    order by EcritureDate,jr_id, j_debit desc
                          ",
            [$this->get_date_start(),$this->get_date_end()]);

        return $operation;

    }
    function get_num_siren()
    {
        return $this->num_siren;
    }

    function set_num_siren($num_siren)
    {
        $this->num_siren=$num_siren;
    }

        function set_from_request()
    {
        parent::set_from_request();
        $http=new \HttpInput();
        $this->num_siren=$http->request("num_siren");
        $this->delimiter=$http->request("delimiter");
    }
    public function export_csv()
    {
        $siren=$this->get_num_siren();
        if ( $this->get_num_siren()=="") {
            $siren=$this->get_date_start();
        }
        $date_end=format_date($this->get_date_end(),"DD.MM.YYYY","YYYYMMDD");
        
        $title=sprintf("%sfec%s",$siren,$date_end);

        $csv=new \NoalyssExport\Output_CSV($title);
        $csv->set_encoding($this->encoding);
        $csv->set_sep_field($this->get_char_delimiter($this->delimiter));

        $aTitle=[
                "JournalCode",
                "JournalLib",
                "EcritureNum",
                "EcritureDate",
                "CompteNum",
                "CompteLib",
                "CompAuxNum",
                "CompAuxLib",
                "PieceRef",
                "PieceDate",
                "EcritureLib",
                "Debit",
                "Credit",
                "EcritureLet",
                "DateLet",
                "ValidDate",
                "Montantdevise",
                "Idevise"
            ];
        $csv->set_encoding($this->encoding);
        $csv->set_sep_field($this->get_char_delimiter($this->delimiter));
        $csv->send_header();
        $csv->write_header($aTitle);
        $aOperation=$this->fetch_data();
        $nb_operation=count($aOperation);
        if ($nb_operation == 0 ) return;
        
        $operation=1;
        $old_operation=$aOperation[0]['jr_id'];
        for ($i = 0;$i < $nb_operation;$i++) {
            if ($old_operation != $aOperation[$i]['jr_id']) {
                $operation++;
                $old_operation=$aOperation[$i]['jr_id'];
            }
            $csv->add ($aOperation[$i]['jrn_def_code']);
            $csv->add ($aOperation[$i]['jrn_def_name']);
            $csv->add ($operation);
            $csv->add ($aOperation[$i]['ecrituredatestr']);
            $csv->add ($aOperation[$i]['j_poste']);
            $csv->add ($aOperation[$i]['pcm_lib']);
            $csv->add ($aOperation[$i]['j_qcode']);
            $csv->add ($aOperation[$i]['f_id_name']);
            $csv->add ($aOperation[$i]['jr_pj_number']);
            $csv->add ($aOperation[$i]['pj_number_date']);
            $csv->add ($aOperation[$i]['jr_comment']);
            $csv->add ($aOperation[$i]['amount_debit'],"number");
            $csv->add ($aOperation[$i]['amount_credit'],"number");
            $csv->add ($aOperation[$i]['lettering']);
            $csv->add ($aOperation[$i]['lettering_date']);
            $csv->add ($aOperation[$i]['validate_date']);
            $csv->add ($aOperation[$i]['oc_amount'],"number");
            $csv->add ($aOperation[$i]['cr_code_iso']);
            $csv->write();
        }

    }
}
