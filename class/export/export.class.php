<?php
namespace NoalyssExport;
/*
 *   This file is part of Noalyss.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2020) Author Dany De Bontridder <dany@alchimerys.be>

require_once NOALYSS_INCLUDE."/lib/noalyss_csv.class.php";

class Export
{
    protected $date_start; //!< date start
    protected $date_end;   //!< date_end
    protected $ledger;  //!< Ledger id (jrn_def.jrn_def_id
    protected $delimiter;
    protected $encoding;
	protected $a_field;
    function __construct()
    {
        global $g_user;
        $cn=\Dossier::connect();
        $exercice=$g_user->get_exercice();
        $per=new \Periode($cn);
        $lim=$per->get_limit($exercice);
        $this->date_start=$lim[0]->first_day();
        $this->date_end=$lim[1]->last_day();
        $this->ledger=0;
        $this->a_field=[';',',',"\t"];
        $aPref=$g_user->get_preference();
        $this->encoding=$aPref["csv_encoding"];
        $this->delimiter=0;
    }
    //--------------------------------------------------------------------
    // Get info from Request
    //--------------------------------------------------------------------
    function set_from_request()
    {
        $http=new \HttpInput();

        $this->date_start=$http->request("date_start", "date");
        $this->date_end=$http->request("date_end", "date");
        $this->encoding=$http->request("csv_encoding");
      
    }
	function get_char_delimiter($p_int)
	{
		return $this->a_field[$p_int];
	}
    /**
     * @return mixed
     */
    public function get_date_start()
    {
        return $this->date_start;
        return $this;
    }

    /**
     * @param mixed $date_start
     */
    public function set_date_start($date_start)
    {
        $this->date_start = $date_start;
        return $this;
    }

    /**
     * @return mixed
     */
    public function get_date_end()
    {
        return $this->date_end;
        return $this;
    }

    /**
     * @param mixed $date_end
     */
    public function set_date_end($date_end)
    {
        $this->date_end = $date_end;
        return $this;
    }
    //--------------------------------------------------------------------
    /// Show a form to input the parameter , ledger , stard and end date
    //--------------------------------------------------------------------
    function input_param()
    {
        $cn=\Dossier::connect();
        $l=new \Acc_Ledger($cn, $this->ledger);
        $select_ledger=$l->select_ledger();

        $date_start=new \IDate("date_start", $this->date_start);
        $date_end=new \IDate("date_end", $this->date_end);
        $csv_encoding=new \ISelect("csv_encoding");
        $csv_encoding->value=array(
            array("label"=>"Latin1", "value"=>"latin1"),
            array("label"=>"Unicode", "value"=>"utf8"),
        );
        $csv_encoding->selected=$this->encoding;

        $delimiter=new \ISelect("delimiter");
        $delimiter->value=array(
            array("label"=>_("Tabulation"), "value"=>"2"),
            array("label"=>_("Virgule"), "value"=>"1"),
            array("label"=>_("Point-virgule"), "value"=>"0")
        );
        $delimiter->selected=$this->get_char_delimiter($this->delimiter);


        require DIR_EXPORT_ACCOUNT."/template/export_param.php";
    }


}
