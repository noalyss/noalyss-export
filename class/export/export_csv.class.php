<?php
namespace NoalyssExport;
/*
 *   This file is part of Noalyss.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2020) Author Dany De Bontridder <dany@alchimerys.be>

require_once DIR_EXPORT_ACCOUNT.'/class/export/export.class.php';

/**
 * @file
 * @brief Export
 */
class Export_CSV extends Export
{


    function __construct()
    {
        parent::__construct();
        $this->ledger=0;
    }

    /**
     * @return int
     */
    public function get_ledger()
    {
        return $this->ledger;
        return $this;
    }

    /**
     * @param int $ledger
     */
    public function set_ledger($ledger)
    {
        $this->ledger = $ledger;
        return $this;
    }


    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    function set_from_request()
    {
        parent::set_from_request();
        $this->ledger=\HtmlInput::default_value_request("p_jrn", 0);
    }

    //--------------------------------------------------------------------
    //--------------------------------------------------------------------
    function export_csv()
    {
        $cn=\Dossier::connect();
        $ledger=new \Acc_Ledger($cn, $this->ledger);
        $cvs=new \Noalyss_Csv("ledger".$ledger->get_name());
        $cvs->set_encoding($this->encoding);
        $type=$ledger->get_type();
        if ($type=='ACH')
        {
            $cvs->send_header();
            $this->export_purchase($cvs);
        }
        else
        if ($type=='VEN')
        {
            $cvs->send_header();
            $this->export_sale($cvs);
        }
        else
        if ($type=="ODS")
        {
            $cvs->send_header();
            $this->export_misc($cvs);
        }
        else
        if ($type=="FIN")
        {
            $cvs->send_header();
            $this->export_fin($cvs);
        }
        else
        {
            throw new Exception(_("Journal invalide"));
        }
    }

    //--------------------------------------------------------------------
    /// Export a ledger of Sale 
    //--------------------------------------------------------------------
    function export_sale(\Noalyss_Csv $p_csv)
    {
        $cn=\Dossier::connect();
        $sql="
        select 
          to_char(jr_date,'DD.MM.YYYY') as sdate,
          jrn_def_code,
          jr_id,
          (select ad_value from fiche_detail where f_id=qs_client and ad_id=23) as qcode,
          jr_pj_number,
          jr_comment,
          (select ad_value from fiche_detail where f_id=qs_fiche and ad_id=23) as qcode_serv,
          qs_unit,
          qs_price,
          qs_vat_code,
          qs_quantite,
          qs_price+qs_vat as price_tax,
          to_char(jr_ech,'DD.MM.YYYY') slimit,
          to_char(jr_date_paid,'DD.MM.YYYY') sdatepaid
      from jrn
      join jrnx on (j_grpt=jr_grpt_id)
      join public.quant_sold using (j_id)
      join jrn_def on (jrn_def_id=jr_def_id)
      where
          jr_date <= to_date($1,'DD.MM.YYYY') and
          jr_date >= to_date($2,'DD.MM.YYYY') and
          jr_def_id=$3
      order by jr_date,j_id
            ";
        $ret=$cn->exec_sql($sql,
                array($this->date_end, $this->date_start, $this->ledger));
        $nb=\Database::num_row($ret);
        for ($i=0; $i<$nb; $i++)
        {
            $row=\Database::fetch_array($ret, $i);
            $p_csv->add($row["sdate"]);
            $p_csv->add($row["jrn_def_code"]);
            $p_csv->add($row["jr_id"]);
            $p_csv->add($row["jr_pj_number"]);
            $p_csv->add($row["qcode"]);
            $p_csv->add($row["jr_comment"]);
            $p_csv->add($row["qcode_serv"]);
            $p_csv->add($row["qs_quantite"], "number");
            $p_csv->add($row["qs_price"], "number");
            $p_csv->add($row["qs_vat_code"]);
            $p_csv->add($row["price_tax"], "number");
            $p_csv->add($row["slimit"]);
            $p_csv->add($row["sdatepaid"]);
            $p_csv->write();
        }
    }

    //--------------------------------------------------------------------
    /// Export a ledger of Purchase 
    //--------------------------------------------------------------------
    function export_purchase(\Noalyss_Csv $p_csv)
    {
        $cn=\Dossier::connect();

        $sql="
        select 
          to_char(jr_date,'DD.MM.YYYY') as sdate,
          jr_id,
          jrn_def_code,
         (select ad_value from fiche_detail where f_id=qp_supplier and ad_id=23) as qcode,
          jr_pj_number,
          jr_comment,
          (select ad_value from fiche_detail where f_id=qp_fiche and ad_id=23) as qcode_serv,
          qp_unit,
          qp_price,
          qp_vat_code,
          qp_quantite,
          qp_price+qp_vat  as price_tax,
          to_char(jr_ech,'DD.MM.YYYY') slimit,
          to_char(jr_date_paid,'DD.MM.YYYY') sdatepaid
        from jrn
            join jrnx on (j_grpt=jr_grpt_id)
            join public.quant_purchase using (j_id)
              join jrn_def on (jrn_def_id=jr_def_id)
        where
          jr_date <= to_date($1,'DD.MM.YYYY') and
          jr_date >= to_date($2,'DD.MM.YYYY') and
          jr_def_id=$3
        order by jr_date,j_id
            ";
        $ret=$cn->exec_sql($sql,
                array($this->date_end, $this->date_start, $this->ledger));
        $nb=\Database::num_row($ret);
        for ($i=0; $i<$nb; $i++)
        {
            $row=\Database::fetch_array($ret, $i);
            $p_csv->add($row["sdate"]);
            $p_csv->add($row["jrn_def_code"]);
            $p_csv->add($row["jr_id"]);
            $p_csv->add($row["jr_pj_number"]);
            $p_csv->add($row["qcode"]);
            
            $p_csv->add($row["jr_comment"]);
            $p_csv->add($row["qcode_serv"]);
            $p_csv->add($row["qp_quantite"], "number");
            $p_csv->add($row["qp_price"], "number");
            $p_csv->add($row["qp_vat_code"]);
            $p_csv->add($row["price_tax"], "number");
            $p_csv->add($row["slimit"]);
            $p_csv->add($row["sdatepaid"]);
            $p_csv->write();
        }
    }

    //--------------------------------------------------------------------
    /// Export a financial ledger
    //--------------------------------------------------------------------
    function export_fin(\Noalyss_Csv $p_csv)
    {
        $cn=\Dossier::connect();
        $sql="
        select 
            jr_id,
            jrn_def_code,
           to_char(jr_date,'DD.MM.YYYY') as sdate,
          (select ad_value from fiche_detail where f_id=qf_other and ad_id=23) as qcode,
           jr_pj_number,
           jr_comment,
           qf_amount
        from 
           quant_fin 
           join jrn using (jr_id)
           join jrn_def on (jrn_def_id=jr_def_id)
        where 
          jr_date <= to_date($1,'DD.MM.YYYY') and
          jr_date >= to_date($2,'DD.MM.YYYY') and
          jr_def_id=$3
          order by jr_date
";
        $ret=$cn->exec_sql($sql,
                array($this->date_end, $this->date_start, $this->ledger));
        $nb=\Database::num_row($ret);
        for ($i=0; $i<$nb; $i++)
        {
            $row=\Database::fetch_array($ret, $i);
            $p_csv->add($row["sdate"]);
            $p_csv->add($row["jrn_def_code"]);
            $p_csv->add($row["jr_pj_number"]);
            
            $p_csv->add($row["jr_id"]);
            $p_csv->add($row["qcode"]);
            $p_csv->add($row["jr_comment"]);
            $p_csv->add($row["qf_amount"]);
            $p_csv->write();
        }
    }

    //--------------------------------------------------------------------
    /// Export ODS ledger
    //--------------------------------------------------------------------
    function export_misc(\Noalyss_Csv $p_csv)
    {
        $cn=\Dossier::connect();
        $sql="
            select 
                to_char(jr_date,'DD.MM.YYYY') as sdate,
                 jrn_def_code,
                jr_id,
                coalesce(j_qcode,j_poste) as qcode,
                jr_pj_number,
                jr_comment,
                j_montant,
                case when j_debit = false then 'C'
                else  'D'
                end as deb
            from 
                jrn 
                join jrnx on (jr_grpt_id=j_grpt)
                  join jrn_def on (jrn_def_id=jr_def_id)
            where
                jr_date <= to_date($1,'DD.MM.YYYY') and
                jr_date >= to_date($2,'DD.MM.YYYY') and
                jr_def_id=$3
            order by jr_date,j_debit desc
";
        $ret=$cn->exec_sql($sql,
                array($this->date_end, $this->date_start, $this->ledger));
        $nb=\Database::num_row($ret);
        for ($i=0; $i<$nb; $i++)
        {
            $row=\Database::fetch_array($ret, $i);
            $p_csv->add($row["sdate"]);
            $p_csv->add($row["jrn_def_code"]);
            $p_csv->add($row["jr_id"]);
            $p_csv->add($row["jr_pj_number"]);
            $p_csv->add($row["qcode"]);
            $p_csv->add($row["jr_comment"]);
            $p_csv->add($row["j_montant"],"number");
            $p_csv->add($row["deb"]);
            $p_csv->write();
        }
    }

}

?>
