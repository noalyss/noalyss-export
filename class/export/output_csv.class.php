<?php
namespace NoalyssExport;
/*
 * Copyright (C) 2015 Dany De Bontridder <dany@alchimerys.be>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


/**
 * @file
 * @brief Manage the CSV : manage files and write CSV record
 *
 */


/**
 * @brief Manage the CSV : manage files and write CSV record
 *
 */
require_once NOALYSS_INCLUDE."/lib/noalyss_csv.class.php";

class Output_CSV extends \Noalyss_Csv
{



    function __construct($p_filename)
    {
        parent::__construct($p_filename);
        $this->set_sep_field("\t");
        $this->set_sep_dec(",") ;
        $this->set_encoding("latin1");

    }

    /***
     * @brief the string are enclosed with  double-quote,
     *  we remove problematic character and
     * the number are formatted.
     * Clean the row after exporting
     * @return nothing
     */
    function write()
    {
        $nb_size=$this->get_size();
        if ($nb_size == 0 ) return;
        $sep="";
        $element=$this->get_element();

        for ($i=0;$i < $nb_size;$i++)
        {
            $export=str_replace("|","",$element[$i]['value']);
            $export=str_replace("\t","",$export);

            if ($element[$i]['type'] == 'number' )
            {
                printf($sep.'%s', $this->nb($export,2));
            }
            else
            {
                // remove break-line,
                $export=str_replace("\n"," ",$export);
                $export=str_replace("\r"," ", $export);
                // remove double quote
                $export=str_replace('"',"", $export);
                printf($sep.'%s', $this->encode($export));
            }
            $sep=$this->get_sep_field();
        }
        printf("\r\n");
        $this->clean();
    }
    function write_header($p_array)
    {
        $size_array=count($p_array);
        $sep="";
        for ($i=0; $i<$size_array; $i++)
        {

            printf($sep.'%s', $this->encode($p_array[$i]));
            $sep=$this->get_sep_field();
        }
        printf("\r\n");
    }

}