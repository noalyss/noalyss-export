<?php

/*
 *   This file is part of Noalyss.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2020) Author Dany De Bontridder <dany@alchimerys.be>

/**
 * @file
 * @brief Form to enter date and ledger for export
 * @see Impacc_export::input_param
 */
require_once  NOALYSS_INCLUDE."/lib/html_tab.class.php";
require_once  NOALYSS_INCLUDE."/lib/output_html_tab.class.php";
?>
<div class="content">
<?php
$span=sprintf('<span  id="%s" class=" icon" >&#xe824;</span>', uniqid());

$csv_tab=new \Html_Tab("csvtab",_("Export  CSV")." ".$span);
$csv_tab->set_comment(_("Permet l'export des opérations en CSV par journal"));
$fec_tab=new \Html_Tab("fectab",_("Export  FEC")." ".$span);
$fec_tab->set_comment(_("Permet l'export des opérations en FEC pour tous les journaux"));
//-----------------------------------------------------------------------------
// export CSV
//-----------------------------------------------------------------------------

$csv_encoding->selected="utf8";
ob_start();
?>
    <div class="notice">
    <?=_("Les séparateurs de champs ainsi que les séparateurs décimales sont dans vos préférences")?>
        
    </div>
<form method="GET" action="extension.raw.php">
    <?php echo \HtmlInput::array_to_hidden(array('gDossier','ac','plugin_code','sa'), $_REQUEST);?>
    <?php echo \HtmlInput::hidden("export_type",'CSV');?>
    <div style="margin-left:25%">
    <ul class="aligned-block" style=width:100%">
        <li style="display:table-row;text-align:right">
            <?php echo _('Journal'); ?>
            <?php echo $select_ledger->input()?>
        </li>
        <li style="display:table-row;text-align:right">
            <?php echo _("Depuis")?>
            <?php echo $date_start->input();?>
        </li>
        <li style="display:table-row;text-align:right">
            <?php echo _("jusque")?>
            <?php echo $date_end->input();?>
        </li>
        <li style="display:table-row;text-align:right">
            <?php echo _("encodage")?>
            <?php echo $csv_encoding->input();?>
        </li>
    </ul>
    <?php echo \HtmlInput::submit("export_operations", _("Export"));?>
    </div>
</form>
    <?php
//-----------------------------------------------------------------------------
// export FEC CSV
//-----------------------------------------------------------------------------
 $csv_tab->set_content(ob_get_contents());
 ob_clean();
 ob_start();
 $date_start->id=uniqid();
 $date_end->id=uniqid();
 $num_siren=new IText("num_siren");
 $csv_encoding->selected="latin1";
 $delimiter->selected=2;
 
?>
    <form method="GET" action="extension.raw.php">
        <?php echo \HtmlInput::hidden("export_type",'FEC');?>
        <p>
          
        </p>
        <?php echo \HtmlInput::array_to_hidden(array('gDossier','ac','plugin_code','sa'), $_REQUEST);?>
        <div style="margin-left:25%">
        <ul class="aligned-block" style="width:100%">
            <li  style="display:table-row;text-align:right">
                <?php echo _("Numéro siren")?>
                <?php echo $num_siren->input();?>
            </li>

            <li style="display:table-row;text-align:right">
                <?php echo _("Depuis")?>
                <?php echo $date_start->input();?>
            </li>
            <li  style="display:table-row;text-align:right">
                <?php echo _("jusque")?>
                <?php echo $date_end->input();?>
            </li>
            <li  style="display:table-row;text-align:right">
				<?php echo _("encodage")?>
				<?php echo $csv_encoding->input();?>
			</li>
			<li style="display:table-row;text-align:right">
				<?php echo _("séparateur")?>
				<?php echo $delimiter->input();?>
        </li>
        </ul>
        <?php echo \HtmlInput::submit("export_operations", _("Export"));?>
        </div>
    </form>
    <?php
    $fec_tab->set_content(ob_get_contents());
    ob_clean();
    $output=new \Output_Html_Tab;
    $output->set_mode("row");

    $output->add($csv_tab);
    $output->add($fec_tab);
    $output->output();
    ?>

</div>
