<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* $Revision$ */

// Copyright (c) 2002 Author Dany De Bontridder dany@alchimerys.be

/* !\file
 * \brief main file for importing card
 */

/*
 * load javascript
 */ 
require_once 'export-constant.php';
$http=new HttpInput();
$version_plugin=\Extension::get_version(__DIR__."/plugin.xml",$http->request("ac"));
global $cn;
echo '<div style="float:right"><a id="help" class="" style="font-size:140%" href="https://gitlab.com/noalyss/noalyss-export/-/wikis/home" target="_blank">Aide</a>'.
'<span style="font-size:0.8em;color:red;display:inline">vers:'.$version_plugin.'</span>'.
'</div>';
$cn=Dossier::connect();
global $version_plugin;
$version_plugin=7;

Extension::check_version(7300);
$plugin_code=$http->request("plugin_code");
$ac=$http->request("ac");
?>
<script>
    var dossier = "<?php echo Dossier::id(); ?>";
    var plugin_code = "<?php echo $plugin_code; ?>";
    var ac = "<?php echo $ac; ?>";
</script>
<?php



$url='?'.dossier::get().'&plugin_code='.$plugin_code."&ac=".$ac;

if ($cn->exist_schema('noaexport')==false)
{
    require_once DIR_EXPORT_ACCOUNT.'/class/install/install.class.php';

    $iplugn=new \NoalyssExport\Install_Plugin();
    $iplugn->install($cn);
}
if ( VERSION_EXPORT > $cn->get_value('select max(id) from noaexport.version') ) {
    require_once DIR_EXPORT_ACCOUNT.'/class/install/install.class.php';

    $iplugin=new NoalyssExport\Install_Plugin();
    $iplugin->upgrade();
}
echo "</div>";
require_once DIR_EXPORT_ACCOUNT.'/class/export/export_csv.class.php';
/**
 * @file
 * @brief Export operation from NOALYSS
 */

/// Step 1 : ask for date + ledger
$export=new \NoalyssExport\Export();
$export->input_param();


