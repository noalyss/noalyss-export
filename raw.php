<?php

/*
 *   This file is part of Noalyss.
 *
 *   Noalyss is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Noalyss is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Noalyss; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2020) Author Dany De Bontridder <dany@alchimerys.be>

require_once 'export-constant.php';
/**
 * @file
 * @brief Export data in raw
 */
require_once DIR_EXPORT_ACCOUNT."/class/export/export_csv.class.php";
require_once DIR_EXPORT_ACCOUNT."/class/export/export_fec_csv.class.php";
$http=new HttpInput();
$export_type=$http->get("export_type");
switch ($export_type)
{
    case "CSV":
        $csv=new \NoalyssExport\Export_CSV();
        try{
            $csv->set_from_request();
            $csv->export_csv();
        } catch ( Exception $e ) {
            header_csv("ERROR.csv");
            printf (_("Erreur : %s"),$e->getMessage());
        }
        break;

        case "FEC":
        $csv=new \NoalyssExport\Export_FEC_CSV();
        try 
        {            
            $csv->set_from_request();
            $csv->export_csv();
                    
        } catch ( Exception $e ) {
            header_csv("ERROR.csv");
            printf (_("Erreur : %s"),$e->getMessage());
        }
        break;
}

?>
