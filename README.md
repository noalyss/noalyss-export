# Description
This application is a plugin for NOALYSS , it is a modified 
version of the Noalyss plugin import_account

Ceci est un plugin pour noalyss, un programme multi-dossier pour la comptabilité
entièrement utilisable avec une interface web.

Plus d'information sur https://www.nolayss.eu

# Installation

Copier l'archive dans noalyss/include/ext et changer le nom du répertoire par 
noalyss-export

Activer le plugin voir : https://wiki.noalyss.eu/doku.php?id=tutoriaux:installer_un_plugin_extension
ou en vidéo https://www.youtube.com/watch?v=myv9MZYdeVw

# Purpose
Export Data, accountancy  to flat file : CSV or FEC

Export les données comptables en CSV ou format FEC
